package main

import (
	"fmt"
	"log"
	"github.com/krotik/eliasdb/graph"
	"github.com/krotik/eliasdb/graph/graphstorage"
	"github.com/krotik/eliasdb/graph/data"
	gql "github.com/krotik/eliasdb/graphql"
)

func main() {
	fmt.Println("main()")
	
	gs, err := graphstorage.NewDiskGraphStorage("db", false)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer gs.Close()

	gm := graph.NewGraphManager(gs)

	node1 := data.NewGraphNode()
	node1.SetAttr("key", "123")
	node1.SetAttr("kind", "mynode")
	node1.SetAttr("name", "Node1")
	node1.SetAttr("text", "The first stored node")
	_ = node1

	gm.StoreNode("main", node1)

	{
		query := map[string]interface{}{
			"operationName": "foo",
			"query": `
{
  Song {
	key
  }
}
`,
			"variables": nil,
		}

		_, err = gql.RunQuery("test", "main", query, gm, nil, false)
		if err == nil || err.Error() != "Fatal GraphQL operation error in test: Missing operation (Operation foo not found) (Line:2 Pos:2)" {
			log.Fatal("Unexpected result:", err)
		
		}

		fmt.Println(err.Error())
	}
}
