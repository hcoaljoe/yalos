from planner import *

## Minimal building planner?
class BuildingPlanner(Planner):
	def __init__(self):
		super(Planner, self)
		# Cm ?
		self.wall_width_side: int = 15
		self.wall_width_top: int = 30
		self.wall_width_bottom: int = 30
		# Meters ?
		self.width: float = 10.0
		self.height: float = 10.0
		self.ceiling_height: float = 2.8

		# Wood, Concrete, Bricks..
		self.wall_material = "wood_frame"

		## Insulation
		# Cm
		self.use_foundation: bool = False
		self.wall_insulation_width_side: int = 15
		self.wall_insulation_width_top: int = 30
		self.wall_insulation_width_bottom: int = 30

		# Mineral wool, polystyrol etc
		self.insulation_material = "mineral_wool"

		## Foundation
		self.use_foundation: bool = False
		# Bricks etc
		self.foundation_material = "brick"
		# Cm
		self.foundation_width: int = 0

	def make_building(self):
		print()
		print("make_building")

		#perimeter_

		print("done")
