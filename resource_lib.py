from resource import FoodResource

resource_lib = {}

def init_resource_lib():
	global resource_lib
	print("init_resource_lib")

	if True:
		r = FoodResource()
		r.name = "dry_peas"
		r.cost = 10
		r.storage_time = 365 * 2

		print("r:", r)

		# Add resource
		add_resource(r)

	if True:
		r = FoodResource()
		r.name = "oat_flakes"
		r.cost = 50
		r.storage_time = 365 / 2

		add_resource(r)

def get_resource(name):
	return resource_lib[name]

## Add resource to resource_lib
def add_resource(r):
	global resource_lib
	resource_lib[r.name] = r
