from planner import *

## Location finder
class LocationFinder:
    def __init__(self):
        self.country_code: str = "by"
        self.region_name: str = "minsk_oblast"

        self.exclude_settlement_types = ["city", "village"]

        # Km
        self.min_distance_from_road: float = 0.0
        self.max_distance_from_road: float = 10.0

        self.min_distance_from_railroad: float = 0.0
        self.max_distance_from_railroad: float = 10.0

        self.min_distance_from_city: float = 0.0
        self.max_distance_from_city: float = 100.0

    def find_location(self):
        print("find_location")

        print("done")

    ## Find location in proximity
    def find_location_near(self):
        print("find_location_near")

        print("done")

    def check_location_near(self, lat, lon: float):
        print("check_location_near:", lat, lon)
        pass

## Location planner/finder
class LocationPlanner(Planner):
    def __init__(self):
        super(Planner, self)
        
    def make_plan(self):
        print()
        print("LocationPlanner make_plan")

        lf = LocationFinder()
        lf.find_location()

        in_locations_list = [
            (12.1232, 93.2812),
        ]

        print("done")
