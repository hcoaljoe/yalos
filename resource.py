from typing import NamedTuple

class Resource(NamedTuple):
	name = ""
	cost = 0.0
	weight = 0.0
	volume = 0.0 # kg/m3 ?

class FoodResource(Resource):
	storage_time = 0 # days
	# Storage time when open / in fridge ?
	storage_time_open = 0 # days
	prepare_time = 0.0 # hours ?
