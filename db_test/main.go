package main

import (
	c "github.com/ostafen/clover"

	"fmt"
)

func main() {
	println("main()")

	db, _ := c.Open("elements-test-db", c.InMemoryMode(true))

	db.CreateCollection("elements")

	el1d := c.NewDocument()
	el2d := c.NewDocument()

	el1d.Set("title", "ldelectus aut autem")
	el1d.Set("completed ", false)

	docId, _ := db.InsertOne("elements", el1d)
	fmt.Println(docId)


	db.Insert("elements", el1d, el2d)

	// Dump
	db.ExportCollection("elements", "dump.json")

	db.DropCollection("elements")

	// Restore collection
	db.ImportCollection("elements", "dump.json")

	elems, _ := db.Query("elements").FindAll()

	for _, el := range elems {
		fmt.Printf("title: %s\n", el.Get("title"))
	}

	// Finally
	db.Close()

	println("done")
}
