#!/usr/bin/env python

import attr

from typing import final, NamedTuple
import dataclasses
from dataclasses import dataclass
from pprint import pprint
from resource import FoodResource
import resource_lib
from resource_lib import get_resource
from planner import *
from foodplanner import FoodPlanner
from buildingplanner import BuildingPlanner
from locationplanner import LocationPlanner

def make_food_plan():
    fp = FoodPlanner()

    ## Set config
    fp.num_persons = 1


    cons_plan = ConsumptionPlan()
    
    e1 = ConsumptionPlanEntity()
    e1.name = "peas dish"
    #e1.res = get_resource("dry_peas")
    #e1.amt = 0.5
    #pprint(e1.res.cost)

    e1.add_resource_rec(get_resource("dry_peas"), 0.5)
    #e1.add_resource_rec(get_resource("dry_peas"), 0.5)
    e1.add_resource_rec(get_resource("oat_flakes"), 0.1)

    e2 = ConsumptionPlanEntity()
    e2.name = "oats dish"
    #e2.res = get_resource("oat_flakes")
    #e2.amt = 0.2

    e2.add_resource_rec(get_resource("oat_flakes"), 0.2)
    #e2.add_resource_rec(get_resource("oat_flakes"), 0.3)

    cons_plan.entities.append(e1)
    cons_plan.entities.append(e2)

    print(len(cons_plan.entities))
    print(len(cons_plan.entities[0].resources))
    print(e1 == e2)
    
    #fp.food_type_res = r
    
    #fp.make_plan(cons_plan, 1)
    fp.make_plan(cons_plan, 2)
    #fp.make_plan(cons_plan, 365)

def make_building_plan():
    ## Buliding planner
    bp = BuildingPlanner()
    bp.make_building()

def make_location_plan():
    lp = LocationPlanner()
    lp.make_plan()

def main():
    print("main()")
    resource_lib.init_resource_lib()
    
    make_food_plan()
    make_building_plan()
    make_location_plan()
    
    print("done")

main()
