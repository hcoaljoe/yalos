module yalos_main

go 1.18

require gitlab.com/hcoaljoe/yalos v0.0.0

require gitlab.com/hcoaljoe/yalos/consplan v0.0.0

replace (
	// XXX local
	//gitlab.com/hcoaljoe/yalos => ./yalos
	gitlab.com/hcoaljoe/yalos v0.0.0 => ./yalos
	gitlab.com/hcoaljoe/yalos/consplan v0.0.0 => ./yalos/consplan
)
