package consplan

import "gitlab.com/hcoaljoe/yalos"

// Consumption plan entity resource record
type EntityResourceRec struct {
    Res *yalos.Resource
    Amt float64
}

func NewEntityResourceRec(res *yalos.Resource, amt float64) *EntityResourceRec {
	rec := &EntityResourceRec{res, amt}

	return rec
}

// XXX ?
func NewEmptyPlanEntityResourceRec() *EntityResourceRec {
	rec := &EntityResourceRec{}

	return rec
}

type ConsumptionSheduleRec struct {
	Name string
	ConsEntity *Entity
	Amt float64
}

// Consumption Plan entity or record ?
type Entity struct {
	Name string
	// XXX make unexported ?
	Resources []*EntityResourceRec

	// Consumption Period
	//Period *ConsPlanPriodRec
}

func NewEntity() *Entity {
	e := &Entity{
		Resources: make([]*EntityResourceRec, 0),
	}

	return e
}

func (e *Entity) AddResourceRec(res *yalos.Resource, amt float64) {
	rec := NewEntityResourceRec(res, amt)
	e.Resources = append(e.Resources, rec)
}

// Resource Consumption Plan
type ConsumptionPlan struct {
	Name string
}

func NewConsumptionPlan() *ConsumptionPlan {
	cp := &ConsumptionPlan{}

	return cp
}
