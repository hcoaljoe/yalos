package main

import (
	"gitlab.com/hcoaljoe/yalos"
	"gitlab.com/hcoaljoe/yalos/consplan"
)

func main() {
	println("main()")

	rLib := yalos.InitResourceLib()

	cp := consplan.NewConsumptionPlan()
	_ = cp

	// XXX make a diet
	e := consplan.NewEntity()
	// Set NameId
	e.Name = "peas dish"
	e.NameId = "peas_dish"
	e.AddResourceRec(rLib.GetResource("dry_peas"), 0.5)
	e.AddResourceRec(rLib.GetResource("oat_flakes"), 0.1)

	cp.AddEntity(e)


	e2 := consplan.NewEntity()
	e2.Name = "oats dish"
	e2.NameId = "oats_dish"
	e2.Description = "oatmeal 400g; weekly"
	e2.Notes = "consumend weekly"
	e2.AddResourceRec(rLib.GetResource("oat_flakes"), 0.4)

	cp.AddEntity(e2)


	cp.Show()


	//cp.SheduleConsumption(time.Everyday, e)
	cp.AddConsumptionShedule("P1D", e) // Once ?
	cp.AddConsumptionShedule("R/P1D", e) // Every one day ?
	cp.AddConsumptionShedule("R-1/P1D", e) // Every one day
	cp.AddConsumptionShedule("R0/P1D", e) // Once ?
	

	//cp.SheduleConsumption(period.Once, e)
	//cp.SheduleConsumption(time.Once, e)
	//cp.SheduleConsumption(time.Everyday, e)
	//cp.SheduleConsumption(time.Everyweek, e2)

	cp.Show()

	
	var _ = `
	cp.AddConsumptionSchedule(time.Everyday, e)
	cp.AddConsumptionSchedule(time.Everyweek, e2)

	s := consplan.NewConsumptionSheduler()

	s.AddPlan(time.Everyday, cp)
	//s.AddEntity(e)

	`
	
	println("done")
}
