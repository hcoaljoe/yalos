package consplan

import (
	"fmt"
	
	"gitlab.com/hcoaljoe/yalos"
)

// Consumption plan entity resource record
type EntityResourceRec struct {
    Res *yalos.Resource
    Amt float64
}

func NewEntityResourceRec(res *yalos.Resource, amt float64) *EntityResourceRec {
	rec := &EntityResourceRec{res, amt}

	return rec
}

// XXX ?
func NewEmptyPlanEntityResourceRec() *EntityResourceRec {
	rec := &EntityResourceRec{}

	return rec
}

// Consumption schedule record ?
//type ConsumptionSheduleRec struct {
type ConsSheduleRec struct {
	Name string
	ConsEntity *Entity
	Amt float64
}

// Consumption Plan entity or record ?
// It is a thing that can be consumed, like consumption recored
// It is similar to bill of materials (?)
type Entity struct {
	Name string
	// Something like slug ?
	NameId string
	//Id string
	Description string
	Notes string
	
	// XXX make unexported ?
	Resources []*EntityResourceRec

	// Consumption Period
	//Period *ConsPlanPriodRec
}

func NewEntity() *Entity {
	e := &Entity{
		Resources: make([]*EntityResourceRec, 0),
	}

	return e
}

func (e *Entity) AddResourceRec(res *yalos.Resource, amt float64) {
	rec := NewEntityResourceRec(res, amt)
	e.Resources = append(e.Resources, rec)
}

// Resource Consumption Plan
type ConsumptionPlan struct {
	Name string

	// Make unexported ?
	entities []*Entity

	// XXX ?
	// shedules / periods ?
	schedules []*ConsSheduleRec
}

func NewConsumptionPlan() *ConsumptionPlan {
	println("NewConsumptionPlan")

	cp := &ConsumptionPlan{
		entities: make([]*Entity, 0),
	}

	return cp
}

func (cp *ConsumptionPlan) AddEntity(e *Entity) {
	cp.entities = append(cp.entities, e)
}

func (cp *ConsumptionPlan) Show() {
	fmt.Println("ConsumptionPlan Show")

	fmt.Println("Entities/records:")
	fmt.Println("     name:      period:")

	for i, e := range cp.entities {
		_ = e
		fmt.Printf("  %d:", i)
		fmt.Printf(" %s %s", e.NameId, e.GetPeriod())
		fmt.Printf("\n")
	}

	fmt.Println("end")
}
