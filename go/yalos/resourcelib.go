package yalos

// XXX firme: move to Context ?
//var gResourceLib map[string]*Resource
var gResourceLib *ResourceLib

type ResourceLib struct {
	resourceMap map[string]*Resource
}

func InitResourceLib() *ResourceLib {
	println("init_resource_lib")

	gResourceLib = NewResourceLib()
	gResourceLib.Init()

	return gResourceLib
}

// XXX
func GetResourceLib() *ResourceLib {
	return gResourceLib
}

func NewResourceLib() *ResourceLib {
	println("NewResourceLib")
	
	rl := &ResourceLib{
		resourceMap: make(map[string]*Resource),
	}

	return rl
}

func (rl *ResourceLib) Init() {
	println("ResourceLib Init")

	if true {
		r := NewResourceType(ResourceType_Food)
		r.Name = "dry_peas"
		r.Cost = 10
		r.FoodResource.StorageTime = 365 * 2

		println("r:", r)

		// Add resource
		rl.AddResource(r)
	}

	if true {
		r := NewResourceType(ResourceType_Food)
		r.Name = "oat_flakes"
		r.Cost = 50
		r.FoodResource.StorageTime = 365 / 2

		rl.AddResource(r)
	}

}

func (rl *ResourceLib) GetResource(name string) *Resource {
	return rl.resourceMap[name]
}

func (rl *ResourceLib) AddResource(r *Resource) {
	rl.resourceMap[r.Name] = r
}
