package yalos


type ResourceI interface {
	GetName() string
}

// entity ?
type Resource struct {
	Name string
	Cost float64
	Weight float64
	Volume float64 // kg/m3 ?

	FoodResource *FoodResource
}

func NewResource() *Resource {
	r := &Resource{}
	
	return r
}

func NewResourceType(typ ResourceType) *Resource {
	r := NewResource()
	
	if typ == ResourceType_Food {
		r.FoodResource = NewFoodResource()
	} else {
		panic("unknow resource type")
	}

	return r
}

func(r *Resource) GetName() string {
	return r.Name
}

// component / record
type FoodResource struct {
	Resource
	StorageTime int // days
	// Storage time when open / in fridge ?
	StorageTimeOpen int // days
	PrepareTime float64 // hours ?
}

// XXX make new record / component ?
func NewFoodResource() *FoodResource {
	fr := &FoodResource{}

	return fr
}
