from pprint import pprint
from planner import *

class FoodPlanner(Planner):
    def __init__(self):
        super(Planner, self)
        self.num_persons = 0
        # XXX multiple types ?
        #self.food_type_res = None

        ## XXX opt
        self.cons_plan = None
        self.time_days = 0

    def make_plan(self, cons_plan: ConsumptionPlan, time_days: int):
        print()
        print("make_plan", time_days)
        ## XXX
        self.cons_plan = cons_plan
        self.time_days = time_days
        self.calc_resources()
        print("done")
        pass

    def calc_resources(self):
        print("calc_resources")
        # Res map
        total_res = {}
        # XXX
        total_res_for_entity = {}

        total_cost = 0.0

        

        for i in range(self.time_days):
            print("-> i:", i)
            # Walk consumption entities
            for cons_en in self.cons_plan.entities:
                #print("-> cons_en:", cons_en)
                # Walk entities' resource records

                ## XXX fixme?
                total_rec_amt = {}
                for rec in cons_en.resources:
                    val = total_rec_amt.get(rec.res.name, 0)
                    total_rec_amt[rec.res.name] = val + rec.amt

                print("total_rec_amt:")
                pprint(total_rec_amt)
                
                for rec in cons_en.resources:
                    #print("-> rec:", rec)
                    res = rec.res
                    res_name = res.name
                    base_h = {
                        "amt": 0.0,
                        # Cost of the resource, static
                        "res_cost": 0.0,
                        # Resulting cost
                        "result": 0.0,
                    }
                    h = total_res.get(res_name, base_h)
                    #h["amt"] += rec.amt
                    h["amt"] = total_rec_amt[res_name]
                    h["res_cost"] = res.cost
                    #h["result"] = res.cost * rec.amt
                    h["result"] = res.cost * h["amt"]
                    total_res[res_name] = h

                    ## total_res_for_entity
                    en_name = cons_en.name
                    print("h:", "res_name:", res_name, "en_name:", en_name)
                    pprint(h)
                    #h2 = total_res_for_entity.get(en_name, h.copy())
                    #h2 = total_res_for_entity.get(en_name, {})
                    h2 = {}
                    h2[res_name] = h.copy()
                    if en_name in total_res_for_entity:
                        x = total_res_for_entity[en_name]
                        x.update(h2)
                        #total_res_for_entity[en_name] = h2
                        total_res_for_entity[en_name] = x
                    else:
                        total_res_for_entity[en_name] = h2

                    ## Calc total cost
                    total_cost += res.cost * rec.amt

        print("total_res:")
        pprint(total_res, width=2, sort_dicts=False)

        print("total_res_for_entity:")
        pprint(total_res_for_entity, width=2, sort_dicts=False)

        print("total_cost:", total_cost)
        #print()

    def check_config(self):
        print("check_config")
        pass
