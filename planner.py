import attr

from typing import final, NamedTuple
import dataclasses
from dataclasses import dataclass

from resource import Resource

# XXX FIXME ? 
class ConsumptionPlanEntity: pass

## Resource Consumption Plan
class ConsumptionPlan:
    def __init__(self):
        self.name = ""
        #plan_period_type = "daily"
        self.entities = []

        self.cons_schedule_recs = []

    ## Schedule consumption for entity ?
    def schedule_consumption(self, time_period, e: ConsumptionPlanEntity) -> None:
        pass

#@final
class ConsumptionPlanEntityResourceRec(NamedTuple):
#@dataclass(frozen=True)
#class ConsumptionPlanEntityResourceRec:
    #res: Resource = None
    res: any = None
    # Day amount, in kg ?
    amt: float = 0.0

#@final
#class ConsumptionPlanEntity(NamedTuple):
#@dataclass(init=False)
#@dataclass(frozen=True)
#@attr.s
#@attr.s(slots=True)
class ConsumptionPlanEntity:
    #__slots__ = ("name", "plan_period_type", "resources")
    #name: str = ""
    #name: str = attr.ib(default="")
    #plan_period_type: str = "daily"
    # ConsumptionPlanEntityResourceRec ?
    #resources: list[ConsumptionPlanEntityResourceRec] = []
    #resources: any = dataclasses.field(default_factory=list)
    #resources: list[ConsumptionPlanEntityResourceRec] = dataclasses.field(default_factory=list)

    def __init__(self):
        self.name: str = ""     
        self.resources: list[ConsumptionPlanEntityResourceRec] = []

    def add_resource_rec(self, res: Resource, amt: float):
        rec = ConsumptionPlanEntityResourceRec(res, amt)
        #rec = ConsumptionPlanEntityResourceRec()
        #rec.res = res
        #rec.amt = amt
        self.resources.append(rec)


class ConsumptionSheduleRec:
    def __init__(self):
        self.name: str = ""
        self.cons_entity: ConsumptionPlanEntity = None
        self.amt: float = 0

class Planner:
    def __init__(self):
        pass
